package com.bai.demo.controller;

import com.bai.demo.model.Student;
import com.bai.demo.util.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class StudentController {
    private static final List<Student> students;

    static {
        students = Arrays.asList(
                new Student(1, "张三", 25, 1, new Date(), "上海朱家嘴"),
                new Student(2, "李四", 33, 0, new Date(), "上海青浦"),
                new Student(3, "王五", 48, 1, new Date(), "上海陆家班"),
                new Student(4, "赵六", 19, 3, new Date(), "上海足球场")
        );
    }

    @RequestMapping(value = "/exportStudentExcel")
    public void exportStudentExcel(HttpServletRequest request, HttpServletResponse response) {
        try {
            ExcelUtil.exportFile(request, response, students, Student.class, "学生信息表");
            System.out.println("excel导出成功~");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("excel导出失败~");
        }
    }

    @RequestMapping(value = {"/", "/index"})
    public ModelAndView goIndex() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }

    @GetMapping(value = "/hello")
    @ResponseBody
    public String hello() {
        return "hello, world";
    }
}
